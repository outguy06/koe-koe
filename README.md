# koe-koe

## depends

This script runs by Python3 and depends on some libraries.
The libraries will be installed by

```
$ pip install --user -r requirements.txt
```

## usage

```
$ ./koe-koe "${URL}"
```

Then, `koe-koe` downloads a mp3 file and add a metadata to the mp3 file.

Enjoy it!
